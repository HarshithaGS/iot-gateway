package neu.hgaddadhar.connecteddevices.project;

import neu.hgaddadhar.connecteddevices.project.MqttClientConnector;
import neu.hgaddadhar.connecteddevices.project.SystemPerformanceAdapter;
import neu.hgaddadhar.connecteddevices.project.common.ConfigConst;
import neu.hgaddadhar.connecteddevices.project.MqttClientConnector;

/**
 * Created on March 20, 2020
 * ActuatorDataSubscriberApp.java: Java class to receive message using MQTT protocol
 * 
 * @author HarshithaGS
 */
public class ActuatorDataSubscriberApp {

	/**
	 * MqttSubClientTestApp - class to receive message using MQTT protocol
	 * 
	 * @var subApp: MQTT subscriber class self instance
	 * @var mqttClient: MQTT connector helper class instance
	 */
	private static ActuatorDataSubscriberApp subApp;
	public static SystemPerformanceAdapter adobj;
	private MqttClientConnector mqttClient;
	private String _host = ConfigConst.DEFAULT_UBIDOTS_SERVER;
	private String _pemFileName = "C:\\Users\\HarshithaGS\\Desktop\\CDLabModules\\iot-gateway\\src\\main\\java\\neu\\hgaddadhar\\connecteddevices\\project\\"
			+ ConfigConst.UBIDOTS + ConfigConst.CERT_FILE_EXT;

	private String _authToken = "BBFF-S8KPvfwXlNLQwE3ooTFToSSDfAZBOV";

	public static final String UBIDOTS_VARIABLE_TEMPERATURE_LABEL = "tempactuator";
	public static final String UBIDOTS_VARIABLE_PRESSURE_LABEL = "pressureactuator";
	public static final String UBIDOTS_VARIABLE_HUMIDITY_LABEL = "humidityactuator";
	public static final String[] UBIDOTS_VARIABLES = new String[] {UBIDOTS_VARIABLE_TEMPERATURE_LABEL, UBIDOTS_VARIABLE_PRESSURE_LABEL, UBIDOTS_VARIABLE_HUMIDITY_LABEL};



	public static final String UBIDOTS_DEVICE_LABEL = "/healthcare";
	public static final String UBIDOTS_TOP_DEFAULT = "/v1.6/devices";

	/**
	 * ActuatorDataSubscriberApp constructor
	 */
	public ActuatorDataSubscriberApp() {
		super();
	}

	/**
	 * Method to start, connect the MQTT subscriber and receive message
	 * 
	 * @param topicName: name of the MQTT session topic in string
	 * @return 
	 */
	public boolean start(String[] topicName) {
		try {
			mqttClient = new MqttClientConnector(_host, _authToken, _pemFileName);
			mqttClient.connect();
			while (true) {
				SystemPerformanceAdapter adobj=new SystemPerformanceAdapter();
				adobj.run();
				Thread.sleep(7);
				mqttClient.subscribeToTopic("/v1.6/devices/healthcare/tempactuator", 2);
				mqttClient.subscribeToTopic("/v1.6/devices/healthcare/pressureactuator", 2);
				mqttClient.subscribeToTopic("/v1.6/devices/healthcare/humidityactuator", 2);
				Thread.sleep(60000); // minimum wait time = 60 sec
			}
		} catch (InterruptedException e) {
			mqttClient.disconnect();
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * Main method of MQTT subscriber class
	 * 
	 * @param args: arguments list
	 */

	public boolean run() {
		subApp = new ActuatorDataSubscriberApp();
		try {
			subApp.start(UBIDOTS_VARIABLES);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return true;
	}


}
