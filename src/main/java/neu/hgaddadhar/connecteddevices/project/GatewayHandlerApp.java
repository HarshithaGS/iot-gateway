package neu.hgaddadhar.connecteddevices.project;

/*
 * GateWayHandlerApp is handling all the connections and instantiating ActuatorDataSubscriberApp
 *  class
 */
public class GatewayHandlerApp {
	public static void main(String args[]) throws InterruptedException {
		ActuatorDataSubscriberApp sub=new ActuatorDataSubscriberApp();
		sub.run();
	}
}