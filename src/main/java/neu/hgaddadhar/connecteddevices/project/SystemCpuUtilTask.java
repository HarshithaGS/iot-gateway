package neu.hgaddadhar.connecteddevices.project;

import java.lang.management.ManagementFactory;
import com.sun.management.OperatingSystemMXBean;
import java.util.logging.Logger;

public class SystemCpuUtilTask {

	Logger logger = Logger.getLogger(SystemCpuUtilTask.class.getName());

	public float getDataFromSensor()
	{
		//calling Max OS bean as Management Factory
		OperatingSystemMXBean osBean =
				(OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		//getting the  cpu load output as float value
		return (float) (osBean.getSystemCpuLoad())*100;

	}
}



