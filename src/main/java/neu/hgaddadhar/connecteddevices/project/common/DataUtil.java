package neu.hgaddadhar.connecteddevices.project.common;

import neu.hgaddadhar.connecteddevices.common.SensorData;
import neu.hgaddadhar.connecteddevices.common.ActuatorData;

import com.google.gson.Gson;
public class DataUtil {
	/**
	 * class having methods to convert sensor data/actuator data to json
	 * and convert json back to sensor data/actuator data
	 */
	
	public DataUtil(){
		SensorData sensordata=new SensorData();
		ActuatorData actuatordata=new ActuatorData();
	}

	public String SensorDataToJson(neu.hgaddadhar.connecteddevices.project.common.SensorData sensorData)
	{
		/**
		 * @param: jsonSd --> String to store the json data
		 * @param: gson --> Gson object
		 * @param: sensordata: Sensor data passed as method argument
		 * @Return: string --> json having sensor data
		 */
		String jsonSd;
		Gson gson = new Gson();
		jsonSd = gson.toJson(sensorData);
		return jsonSd;
	}
	public SensorData JsonToSensorData(String jsondata)
	{
		/**
		 * @param: sensorData --> SensorData object to store the sensor data from json data
		 * @param: gson --> Gson object
		 * @param: jsondata --> json data passed as method argument
		 * @return: SensorData --> sensor data converted from json data
		 */
		SensorData sensorData;
		Gson gson = new Gson();
		sensorData = gson.fromJson(jsondata, SensorData.class);
		return sensorData;
	}	
	
	public String ActuatorDataToJson(ActuatorData actuatordata)
	{
		/**
		 * @param: jsonSd --> String to store the json data
		 * @param: gson --> Gson object
		 * @param: actuatordata: Actuator data passed as method argument
		 * @Return: string --> json having actuator data
		 */
		String jsonSd;
		Gson gson = new Gson();
		jsonSd = gson.toJson(actuatordata);
		return jsonSd;
	}
	public ActuatorData JsonToActuatorData(String jsondata)
	{
		/**
		 * @param: actuatorData --> ActuatorData object to store the sensor data from json data
		 * @param: gson --> Gson object
		 * @param: jsondata --> json data passed as method argument
		 * @return: ActuatorData --> actuator data converted from json data
		 */
		ActuatorData actuatorData;
		Gson gson = new Gson();
		actuatorData = gson.fromJson(jsondata, ActuatorData.class);
		return actuatorData;
	}	
	
	
	
}
