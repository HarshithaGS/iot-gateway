package neu.hgaddadhar.connecteddevices.project.common;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SensorData implements Serializable {
	/**
	* store temperature sensor data
	*/
// Static variable
	private static final long serialVersionUID = 1L;
// private variables 
	private String timeStamp = null;
	private float curVal = 0.0f;
	private float avgVal = 0.0f;
	private float minVal = 0.0f;
	private float maxVal = 0.0f;
	private float totVal = 0.0f;
	private int sampleCount = 0;
	private String name="SensorData";
	private float nominalVal=6;

	// constructors
	/**
	 * Default.
	 *
	 */
	public SensorData() {
		super();
		updateTimeStamp();
	}

	public SensorData(String name)
	{
		this.name=name;
	}
	public float getnominalVal()
	{
		return nominalVal;
				
	}
	/**
	 * add current temperature to data 
	 * and update other parameters : minVal, maxVal, totVal
	 * @param val
	 */
	public void addValue(float val) {
		updateTimeStamp(); // update the time
		++sampleCount; // increase sample's value
		curVal = val; // set current value 
		totVal += val; // total value updated
		if (curVal < minVal) {
			minVal = curVal;
		}
		if (curVal > maxVal) {
			maxVal = curVal;
		}
		if (totVal != 0 && sampleCount > 0) {
			avgVal = totVal / sampleCount;
		}
		if (this.sampleCount == 1) {
		    this.minVal = this.curVal;
		} else if (this.curVal < this.minVal) {
		    this.minVal = this.curVal;
		}
	}
	
	//update current time in specified format
	public void updateTimeStamp() {
		timeStamp = new SimpleDateFormat("yyyy.MM.dd HH:mm.ss").format(new Date());
	}
	
	//print sensor data
	public String getData()
	{
		String st;
		st = (
				"time: " + this.timeStamp + "\n" +
				"Current Temperature: " + this.curVal + "\n" +
				"Average Temperature: " + this.avgVal + "\n" +
				"Sample number: " + this.sampleCount + "\n" +
				"Min: " + this.minVal+ "\n" +
				"Max: " + this.maxVal+ "\n" 
		);
		return st;
	}
	//returns sensor data in Byte array
	public byte[] getBytes()

	{
		return getData().getBytes();
	}
	
	/**
	 * Getters and Setters for private variables
	 */
	public float getavgVal() {
		return avgVal;
	}

	public float getmaxVal() {
		return maxVal;
	}

	public float getminVal() {
		return minVal;
	}
	
	public float getValue() {
		return curVal;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public float getcurVal() {
		return curVal;
	}

	public void setcurVal(float curVal) {
		this.curVal = curVal;
	}

	public float gettotVal() {
		return totVal;
	}

	public void settotVal(float totVal) {
		this.totVal = totVal;
	}

	public int getsampleCount() {
		return sampleCount;
	}

	public void setsampleCount(int sampleCount) {
		this.sampleCount = sampleCount;
	}

	public void setavgVal(float avgVal) {
		this.avgVal = avgVal;
	}

	public void setminVal(float minVal) {
		this.minVal = minVal;
	}

	public void setmaxVal(float maxVal) {
		this.maxVal = maxVal;
	}
	
	/*
	 * Function adds current value to calculate average value
	 * @param val - Current sensor value
	 */
	public void updateValue(float val) {
		updateTimeStamp();
		++this.sampleCount;
		this.curVal = (float) val;
		this.totVal += val;
		if (this.curVal < this.minVal) {
			this.minVal = this.curVal;
		}
		if (this.curVal > this.maxVal) {
			this.maxVal = this.curVal;
		}
		if (this.totVal != 0 && this.sampleCount > 0) {
			this.avgVal = this.totVal / this.sampleCount;
		}
	}
}