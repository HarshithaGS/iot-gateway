package neu.hgaddadhar.connecteddevices.project.common;

//ConfigConst class to provide easy access from properties file 
public class ConfigConst
{
	public static final String SECTION_SEPARATOR        = ".";
	public static final String DATA_PATH                = "C:\\Users\\HarshithaGS\\Desktop\\CDLabModules\\iot-gateway\\config";
	public static final String DEFAULT_CONFIG_FILE_NAME = DATA_PATH + "ConnectedDevicesConfig.props";
	public static final String CLOUD                  = "cloud";
	public static final String SMTP                   = "smtp";
	public static final String COAP                   = "coap";
	public static final String GATEWAY_DEVICE         = "gateway";
	public static final String SMTP_CLOUD_SECTION     = SMTP    + SECTION_SEPARATOR + CLOUD;
	public static final String COAP_GATEWAY_SECTION   = COAP    + SECTION_SEPARATOR + GATEWAY_DEVICE;
	public static final Object SMTP_PROP_HOST_KEY = "mail.smtp.host";
    public static final Object SMTP_PROP_PORT_KEY ="mail.smtp.port";
    public static final Object SMTP_PROP_ENABLE_TLS_KEY ="mail.smtp.starttls.enable";
    public static final Object SMTP_PROP_AUTH_KEY="mail.smtp.auth";
	public static final String FROM_ADDRESS_KEY       = "fromAddr";
	public static final String TO_ADDRESS_KEY         = "toAddr";
	public static final String TO_MEDIA_ADDRESS_KEY   = "toMediaAddr";
	public static final String TO_TXT_ADDRESS_KEY     = "toTxtAddr";
	public static final String HOST_KEY               = "host";
	public static final String PORT_KEY               = "port";
	public static final String SECURE_PORT_KEY        = "securePort";
	public static final String USER_AUTH_TOKEN_KEY    = "authToken";
	public static final String BASE_URL_KEY           = "baseUrl";
	public static final String ENABLE_AUTH_KEY        = "enableAuth";
	public static final String ENABLE_CRYPT_KEY       = "enableCrypt";
	public static final String CONFIG_FILE_PARAM      = "configFile";
	public static final String DEVICE                 = "device";
	
	public static final String REDIS_HOST			  = "rHost";
	public static final String REDIS_PORT			  = "rPort";
	public static final String REDIS_PASSWORD		  = "rPwd";
	
	public static final String MQTT                   = "mqtt";
	public static final String MQTT_CLOUD_SECTION 	  = MQTT + SECTION_SEPARATOR + CLOUD;
	public static final String DEFAULT_MQTT_PROTOCOL  = "tcp";
	public static final String SECURE_MQTT_PROTOCOL   = "ssl";
	public static final String DEFAULT_MQTT_SERVER    = "mqtt.eclipse.org";
	public static final int    DEFAULT_MQTT_PORT      = 1883;
	public static final int    SECURE_MQTT_PORT       = 8883;
	public static final int    DEFAULT_QOS_LEVEL      =    0;
	
	public static final String CERT_FILE_EXT          = "_cert.pem";
	public static final String UBIDOTS                = "ubidots";
	public static final String DEFAULT_UBIDOTS_SERVER = "industrial.api.ubidots.com";
	public static final String UBIDOTS_CERT_FILE      = DATA_PATH + UBIDOTS + CERT_FILE_EXT;
	public static final String UBIDOTS_CLOUD_SECTION  = UBIDOTS + SECTION_SEPARATOR + CLOUD;

}