package neu.hgaddadhar.connecteddevices.project.common;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.configuration.*;


public class ConfigUtil
{
	// static
	
	private static final Logger _Logger =Logger.getLogger("Configuration");
	
	private static final ConfigUtil _Instance = new ConfigUtil();

	/**
	 * Returns the Singleton instance of this class.
	 * @return ConfigUtil
	 */
	public static final ConfigUtil getInstance()
	{
		return _Instance;
	}
	
	
	// private variables
	
	private HierarchicalINIConfiguration _sectionProperties = null;
	
	private boolean _isLoaded = false;
	
	
	// constructors
	
	/**
	 * Default (private)
	 * Creates a new instance of {@link HierarchichalINIConfiguration}.
	 */
	public ConfigUtil()
	{
		super();
		
		_sectionProperties = new HierarchicalINIConfiguration();
	}
	
	
	// public methods
	
	/**
	 * Returns the requested property from the given section
	 */
	public String getProperty(String section, String propName)
	{
		SubnodeConfiguration subNodeConfig = _sectionProperties.getSection(section);
		
		return subNodeConfig.getString(propName);
	}
	
	/**
	 * Returns the requested property from the given section.
	 */
	public boolean getBooleanProperty(String section, String propName)
	{
		SubnodeConfiguration subNodeConfig = _sectionProperties.getSection(section);
		
		return subNodeConfig.getBoolean(propName);
	}
	
	/**
	 * Returns the requested property from the given section.
	 */
	public int getIntegerProperty(String section, String propName)
	{
		SubnodeConfiguration subNodeConfig = _sectionProperties.getSection(section);
		
		return subNodeConfig.getInt(propName);
	}
	
	/**
	 * Returns the requested property from the given section.
	 * 
	 */
	public float getFloatProperty(String section, String propName)
	{
		SubnodeConfiguration subNodeConfig = _sectionProperties.getSection(section);
		
		return subNodeConfig.getFloat(propName);
	}
	
	/**
	 * Attempts to load the default configuration file.
	 * @return boolean True on success; false otherwise.
	 */
	public boolean loadConfig()
	{
		return loadConfig(ConfigConst.DEFAULT_CONFIG_FILE_NAME);
	}
	
	/**
	 * Attempts to load the default configuration file, as specified
	 * @return boolean True on success; false otherwise.
	 */
	public boolean loadConfig(String configFile)
	{
		boolean success = false;
		
		try {
			if (configFile == null || configFile.trim().length() == 0) {
				configFile = ConfigConst.DEFAULT_CONFIG_FILE_NAME;
				
				_Logger.warning("Config file is invalid. Using default: " + configFile);
			}
			
			_sectionProperties.load(configFile);
			_isLoaded = true;
		} catch (ConfigurationException e) {
			_Logger.log(Level.WARNING, "Failed to load configuration file: " + configFile, e);
		}
		
		success = _isLoaded;
		
		if (! success) {
			_Logger.warning(
				"Config file not loaded: " + configFile + ". See stack trace for failure details.");
		}
		
		return success;
	}
	
	/**
	 * Returns the flag indicating if either of the most recently called
	 * @return true True if the most recent load invocation was successful;
	 * false otherwise.
	 */
	public boolean isConfigFileLoaded()
	{
		return _isLoaded;
	}
	
}