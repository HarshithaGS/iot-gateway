package neu.hgaddadhar.connecteddevices.project;


/*
 * ManagementFactory - Provides the management interfaces for monitoring and management of the Java virtual machine and other components in the Java runtime.
 * MemoryMXBean - management interface for the memory system of the Java virtual machine.
 */
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.util.logging.Logger;


public class SystemMemUtilTask {
	Logger logger 
	= Logger.getLogger( 
			SystemMemUtilTask.class.getName());

	public float getDataFromSensor()
	{
		MemoryMXBean memBean = ManagementFactory.getMemoryMXBean();
		//calling the  heap memory
		MemoryUsage heap= memBean.getHeapMemoryUsage();
		//calling  the  non-heap memory
		MemoryUsage nonheap=memBean.getNonHeapMemoryUsage();
		//getting the  heap memory usage
		MemoryUsage memUsage=ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
		//calculating the  heap utilization
		double heapUtil=((double) heap.getUsed()/heap.getMax())*100;
		double nonheapUtil=((double) nonheap.getUsed()/heap.getMax())*100;

		if (heapUtil < 0.0d) heapUtil =0.0d;
		if (nonheapUtil < 0.0d) nonheapUtil =0.0d;

		//return the  heap utilization percentage
		return (float)heapUtil*100;
	}
}
