package neu.hgaddadhar.connecteddevices.project;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger; 

public class SystemPerformanceAdapter extends Thread {

	Logger logger = Logger.getLogger(SystemMemUtilTask.class.getName());

	SystemCpuUtilTask cpu = new SystemCpuUtilTask();
	SystemMemUtilTask mem=new SystemMemUtilTask();

	public void run() 
	{ 
		
		try
		{ 	//reference to cpu method     
			String strcpu = String.valueOf(cpu.getDataFromSensor());
			logger.info("\tCPU Utilization Percent:   "+strcpu);
			sleep(2);
			//reference to memory method  
			String strmem = String.valueOf(mem.getDataFromSensor());
			logger.info("\tMemory Utilization Percent:   "+strmem);
			sleep(2);
		} 
		catch (Exception e) 
		{ 
			// Throwing an exception 
			System.out.println ("Exception is caught"); 
		} 
	
	} 
} 

// Main Class 
 class SystemPerformanceAdapterApp
{ 
	public void runfinal() 
	{ 
		int n = 10; // Number of threads 
		for (int i=0; i<10; i++) 
		{ 
			SystemPerformanceAdapter object = new SystemPerformanceAdapter();
			try {
				TimeUnit.SECONDS.sleep(5);
				object.start();  
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
	}
}