package neu.hgaddadhar.connecteddevices.common;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SensorData implements Serializable {
	/**
	 * store temperature sensor data
	 */
	private static final long serialVersionUID = 1L;
	// private variables 
	private String timeStamp = null;
	private float curValue = 0.0f;
	private float avgValue = 0.0f;
	private float minValue = 0.0f;
	private float maxValue = 0.0f;
	private float totValue = 0.0f;
	private int samples = 0;
	private String name = "Temperature Emulator ";

	// constructors
	public SensorData() {
		super();
		updateTimeStamp();
	}

	/**
	 * add current temperature to data 
	 * and accordingly update avg, max and min temp value
	 * @param val
	 */
	public void addValue(float val) {
		updateTimeStamp(); // update the time
		++samples; // increase sample value
		curValue = val; // set current value 
		totValue += val; // total value updated
		if (curValue < minValue) {
			minValue = curValue;
		}
		if (curValue > maxValue) {
			maxValue = curValue;
		}
		if (totValue != 0 && samples > 0) {
			avgValue = totValue / samples;
		}


		if (this.samples == 1) {
			this.minValue = this.curValue;
		} else if (this.curValue < this.minValue) {
			this.minValue = this.curValue;
		}
	}

	/**
	 * Getters and Setters for private variables
	 */
	public float getAvgValue() {
		return avgValue;
	}

	public float getMaxValue() {
		return maxValue;
	}

	public float getMinValue() {
		return minValue;
	}

	public float getcurValue() {
		return curValue;
	}

	/**
	 *Updates current time
	 */
	public void updateTimeStamp() {
		timeStamp = new SimpleDateFormat("yyyy.MM.dd HH:mm.ss").format(new Date());
	}

	/**
	 * Print sensordata object
	 */
	public String toString()
	{
		String st;
		st = (
				"time: " + timeStamp + "\n" +
						"Current Temperature: " + curValue + "\n" +
						"Average Temperature: " + avgValue + "\n" +
						"Sample number: " + samples + "\n" +
						"Min: " + minValue+ "\n" +
						"Max: " + maxValue+ "\n" 
				);
		return st;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public float getCurValue() {
		return curValue;
	}

	public void setCurValue(float curValue) {
		this.curValue = curValue;
	}

	public float getTotValue() {
		return totValue;
	}

	public void setTotValue(float totValue) {
		this.totValue = totValue;
	}

	public int getSamples() {
		return samples;
	}

	public void setSamples(int samples) {
		this.samples = samples;
	}

	public void setAvgValue(float avgValue) {
		this.avgValue = avgValue;
	}

	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}


	public String getName() {
		return name;
	}

}