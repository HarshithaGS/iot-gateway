package neu.hgaddadhar.connecteddevices.common;
//imports
import java.io.Serializable;

import org.joda.time.DateTime;


//This class is defined to create an Actuator Data in order to  interact with Raspberry Pi and the SenseHat
public class ActuatorData  implements Serializable{

	// private variables 
	public int COMMAND_OFF = 0;
	public int COMMAND_ON = 1;
	public int COMMAND_SET = 2;
	public int COMMAND_RESET = 3;
	public int	STATUS_IDLE = 0;
	public int	STATUS_ACTIVE = 1;
	public int	ERROR_OK = 0;
	public int	ERROR_COMMAND_FAILED = 1;
	public int	ERROR_NON_RESPONSIBLE = -1;

	private static final long serialVersionUID = 1L;
	private String timeStamp = null;
	private int command = 0;
	private int errcode = 0;
	private int statuscode = 0;
	private String stateData = null;

	private String name = "Alert message";
	private float curValue  = 0.0f;
	private float nominalval = 0.0f;

	//constructor
	public ActuatorData() {
		super();
		timeStamp=DateTime.now().toString();
	}

	//    This function returns the value of command
	public int getCommand() {
		return command;
	}

	//This function sets the value of command
	public void setCommand(int command) {
		this.command = command;
	}

	//This function returns value of Errcode
	public int getErrcode() {
		return errcode;
	}

	//This function sets the value of Errcode
	public void setErrcode(int errcode) {
		this.errcode = errcode;
	}

	//This function returns StatusCode
	public int getStatuscode() {
		return statuscode;
	}

	//This function sets the value of StatusCode
	public void setStatuscode(int statuscode) {
		this.statuscode = statuscode;
	}

	//This function gets the State Data
	public String getStateData() {
		return stateData;
	}

	//This function sets the StateData
	public void setStateData(String stateData) {
		this.stateData = stateData;
	}

	//This function return Name
	public String getName() {
		return name;
	}

	//This functions sets Name
	public void setName(String name) {
		this.name = name;
	}

	//This  function returns Value
	public float getVal() {
		return curValue ;
	}

	// This function sets the Value
	public void setVal(float curValue ) {
		this.curValue  = curValue ;
	}

	//This function returns the Nominal Value
	public float getNominalval() {
		return nominalval;
	}

	//This function sets the Nominal Value
	public void setNominalval(float nominalval) {
		this.nominalval = nominalval;
	}

	// function to print in Human Readable format
	public String getString()
	{
		String st;
		st = (
				"time: " + timeStamp + "\n" +
						"Command: " + command + "\n" +
						"errcode: " + errcode + "\n" +
						"stateData: " + statuscode + "\n" +
						"val: " + curValue + "\n" 

				);
		return st;
	}
}
