package neu.hgaddadhar.connecteddevices.common;

import neu.hgaddadhar.connecteddevices.common.SensorData;
import neu.hgaddadhar.connecteddevices.common.ActuatorData;
import com.google.gson.Gson;

//This class handles conversions of SensorData and ActuatorData to / from JSON for use within the Redis database.

public class DataUtil {
	/**
	 * class having methods to convert sensor data to json
	 * and convert json back to sensor data
	 */
	//constructor
	public DataUtil(){
		SensorData sensordata=new SensorData();
		ActuatorData actuatordata=new ActuatorData();
	}

	//  Converting Sensor Data to Json format
	public String SensorDataToJson(SensorData sensordata)
	{
		/**
		 * @param: jsonSd --> String to store the json data
		 * @param: gson --> Gson object
		 * @param: sensordata: Sensor data passed as method argument
		 * @Return: string --> json having sensor data
		 */
		String jsonSd;
		Gson gson = new Gson();
		jsonSd = gson.toJson(sensordata);
		return jsonSd;
	}

	//Converting Json format of Sensor Data to Sensor Data format
	public SensorData JsonToSensorData(String jsondata)
	{
		/**
		 * @param: sensorData --> SensorData object to store the sensor data from json data
		 * @param: gson --> Gson object
		 * @param: jsondata --> json data passed as method argument
		 * @return: SensorData --> sensor data converted from json data
		 */
		SensorData sensorData;
		Gson gson = new Gson();
		sensorData = gson.fromJson(jsondata, SensorData.class);
		return sensorData;
	}

	//Converting Actuator Data to Json format
	public String ActuatorDataToJson(ActuatorData actuatordata)
	{
		/**
		 * @param: jsonSd --> String to store the json data
		 * @param: gson --> Gson object
		 * @param: sensordata: Sensor data passed as method argument
		 * @Return: string --> json having sensor data
		 */
		String jsonAd;
		Gson gson = new Gson();
		jsonAd = gson.toJson(actuatordata);
		return jsonAd;
	}
	
	//Converting Json format of Actuator Data to Sensor Data format
	public ActuatorData JsonToActuatorData(String jsondata)
	{
		/**
		 * @param: sensorData --> SensorData object to store the sensor data from json data
		 * @param: gson --> Gson object
		 * @param: jsondata --> json data passed as method argument
		 * @return: SensorData --> sensor data converted from json data
		 */
		ActuatorData actuatorData;
		Gson gson = new Gson();
		actuatorData = gson.fromJson(jsondata, ActuatorData.class);
		return actuatorData;
	}
}