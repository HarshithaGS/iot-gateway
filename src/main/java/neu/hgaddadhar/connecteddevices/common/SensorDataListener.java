package neu.hgaddadhar.connecteddevices.common;

import neu.hgaddadhar.connecteddevices.common.PersistenceUtil;



/*Listens to the Redis and prints a statement saying it can read all the Sensor Data from Redis once received*/

public class SensorDataListener {

	PersistenceUtil putil = new PersistenceUtil();
	public SensorDataListener() {
		this.putil = putil;
	}
	//Check flag for Sensor Data ACcess
	public void onMessage() {
		if(this.putil.flag==1) {
			System.out.println(" Sensor Data Received from Redis");
		}
	}

}
