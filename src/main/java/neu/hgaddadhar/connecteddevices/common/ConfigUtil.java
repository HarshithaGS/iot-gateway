package neu.hgaddadhar.connecteddevices.common;

import java.io.File;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.configuration.*;
//This Class helps in loading , reading and getting properties or values from the default configuration file
public class ConfigUtil
{
	// static

	private static final Logger     _Logger = Logger.getLogger(ConfigUtil.class.getName());

	private static final ConfigUtil _Instance = new ConfigUtil();

	/**
	 * Returns a Singleton instance of this class.
	 */
	public static final ConfigUtil getInstance()
	{
		return _Instance;
	}


	// private var's

	private HierarchicalINIConfiguration _sectionProperties = null;

	private boolean _isLoaded = false;


	// constructors

	/**
	 * Default (private).
	 * 
	 * Creates a new instance of {@link HierarchichalINIConfiguration}.
	 */
	public  ConfigUtil()
	{
		super();

		initBackingProperties();
	}


	// public methods

	/**
	 * Returns the requested property from the given section.
	 * 
	 */
	public synchronized String getProperty(String section, String propName)
	{
		SubnodeConfiguration subNodeConfig = _sectionProperties.getSection(section);

		return subNodeConfig.getString(propName);
	}

	/**
	 * Returns the requested property from the given section.
	 * 
	 */
	public synchronized boolean getBooleanProperty(String section, String propName)
	{
		SubnodeConfiguration subNodeConfig = _sectionProperties.getSection(section);

		return subNodeConfig.getBoolean(propName);
	}

	/**
	 * Returns the requested property from the given section.
	 */
	public synchronized int getIntegerProperty(String section, String propName)
	{
		SubnodeConfiguration subNodeConfig = _sectionProperties.getSection(section);

		return subNodeConfig.getInt(propName);
	}

	/**
	 * Returns the requested property from the given section.
	 */
	public synchronized float getFloatProperty(String section, String propName)
	{
		SubnodeConfiguration subNodeConfig = _sectionProperties.getSection(section);

		return subNodeConfig.getFloat(propName);
	}

	/**
	 * Returns true if the requested property exists in the given section.
	 */
	public synchronized boolean hasProperty(String section, String propName)
	{
		SubnodeConfiguration subNodeConfig = _sectionProperties.getSection(section);

		return (subNodeConfig.getProperty(propName) != null);
	}

	/**
	 * Returns true if the requested section exists and / or has
	 * any properties 
	 */
	public synchronized boolean hasSection(String section)
	{
		SubnodeConfiguration subNodeConfig = _sectionProperties.getSection(section);

		return (subNodeConfig != null);// && subNodeConfig.isEmpty());
	}

	/**
	 * trues to load the default configuration file. 
	 */
	public synchronized boolean loadConfig()
	{
		return loadConfig(ConfigConst.DEFAULT_CONFIG_FILE_NAME);
	}

	/**
	 * tries to load the default configuration file
	 * 
	 */
	public synchronized boolean loadConfig(String configFile)
	{
		boolean success = false;

		try {
			if (configFile == null || configFile.trim().length() == 0) {
				configFile = ConfigConst.DEFAULT_CONFIG_FILE_NAME;

				_Logger.warning("Config file is invalid. Using default: " + configFile);
			}

			// ensure config file exists
			File cfgFile = new File(configFile);

			if (cfgFile.exists() && cfgFile.canRead()) {
				// init the backing properties, or clear out the existing one
				initBackingProperties();

				_sectionProperties.load(configFile);
				_isLoaded = true;
			} else {
				_Logger.warning("Config file either doesn't exist or can't be read.");
			}
		} catch (ConfigurationException e) {
			_Logger.log(Level.WARNING, "Failed to load configuration file: " + configFile, e);
		}

		success = _isLoaded;

		if (! success) {
			_Logger.warning(
					"Config file not loaded: " + configFile + ". See stack trace for failure details.");
		}

		return success;
	}

	/**
	 * Returns the flag indicating if either of the most recently called
	 * loadConfig methods are invoked
	 */
	public boolean isConfigDataLoaded()
	{
		return _isLoaded;
	}

	// private methods

	/**
	 * Initializes the backing properties store 
	 * 
	 */
	private void initBackingProperties()
	{
		_sectionProperties = new HierarchicalINIConfiguration();
	}

}
