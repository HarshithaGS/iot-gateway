package neu.hgaddadhar.connecteddevices.common;
//import
import neu.hgaddadhar.connecteddevices.common.DataUtil;
import neu.hgaddadhar.connecteddevices.common.ConfigConst;
import neu.hgaddadhar.connecteddevices.common.ConfigUtil;
import neu.hgaddadhar.connecteddevices.common.SensorData;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import neu.hgaddadhar.connecteddevices.common.ActuatorData;


/**This class handles the following:
1. Uses DataUtil to handle conversions of SensorData and ActuatorData to / from JSON for use within the Redis database.
2. Read SensorData to Redis
3. Writes to  Redis the Actuator Data
 */
public class PersistenceUtil {

	boolean test = ConfigUtil.getInstance().loadConfig(ConfigConst.DEFAULT_CONFIG_FILE_NAME);

	String redis_host= (ConfigUtil.getInstance().getProperty(ConfigConst.REDIS, ConfigConst.REDIS_HOST));
	Integer redis_port= Integer.valueOf((ConfigUtil.getInstance().getProperty(ConfigConst.REDIS, ConfigConst.REDIS_PORT)));
	String redis_auth= (ConfigUtil.getInstance().getProperty(ConfigConst.REDIS, ConfigConst.REDIS_AUTH));

	Jedis jedis = new Jedis();	//Jedis Instnace
	JedisPool jedispool = new JedisPool (redis_host ,redis_port);

	DataUtil util=new DataUtil();
	SensorData s=new SensorData();
	ActuatorData a=new ActuatorData();
	int i=1;
	int j=1;
	int flag =0;

	//Constructor
	public PersistenceUtil()
	{	
		this.jedispool = jedispool;
		this.jedis =jedispool.getResource();
		this.jedis.auth(redis_auth);
		System.out.println("Connection Successful");
		this.util=util;

	}

	//Function to read Sensor Data from Redis
	public SensorData ReadFromRedis()
	{
		int id_count =0;
		String key="sample" + i;
		String value=jedis.get(key);
		s = util.JsonToSensorData(value);
		id_count =1;
		i+=1;
		return s;
	}

	//Function to Write Actuator Data to Redis
	public void WriteToRedis(ActuatorData a)
	{
		int id_count =0;
		String key="actuate" + j;
		String Actuatorjson1 = new String();
		Actuatorjson1 = util.ActuatorDataToJson(a);
		jedis.set(key, Actuatorjson1);
		System.out.println("Actuated");
		id_count =1;
		j+=1;
	}

	//This function sets a flag to Python to say that all the ACtuator Data is written into redis for device to access
	public void WriteToRedisFlag(String key) {
		jedis.set(key ,"yes");
		System.out.println("Actuated Data written to Redis.");
	}
}

