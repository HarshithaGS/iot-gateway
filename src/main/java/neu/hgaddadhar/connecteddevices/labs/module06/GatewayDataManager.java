package neu.hgaddadhar.connecteddevices.labs.module06;

import neu.hgaddadhar.connecteddevices.common.DataUtil;
import neu.hgaddadhar.connecteddevices.common.SensorData;
import java.util.logging.Logger;
import neu.hgaddadhar.connecteddevices.labs.module06.MqttClientConnector;

/*
 * This class will start the connection and listen to the channel waiting to suscribe the data published
 */
public class GatewayDataManager {

	private static final Logger logger = Logger.getLogger(GatewayDataManager.class.getName());
	private static GatewayDataManager Gdm;
	private MqttClientConnector mqttClient;

	//constructor
	public GatewayDataManager() {
		super();
	}

	//This subscribe method takes the topic name and qos assigned in order to start the connection and subscribing
	public void subscribe(String topicName) {
		mqttClient = new MqttClientConnector();
		mqttClient.setCallback(new GatewayDataManager()); //a pointer to a message callback function called when a message arrives for a subscription created by this client.
		//start the connection
		mqttClient.connect();
		mqttClient.subscribeToTopic(topicName, 2);
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// This method calls the subscribe method and prints the data before and after converting from JSON
	public boolean run() {

		Gdm = new GatewayDataManager();
		String topic = "SensorData"; // Topic name is set
		SensorData sensor = null;
		try {
			Gdm.subscribe(topic);
			String message = MqttClientConnector.getMessag(); // Retrieving Json Data
			logger.info("Received the SensorData in Json Format\n");
			System.out.println("Received the SensorData in Json Format: " + message + "\n");
			DataUtil data = new DataUtil();
			sensor = data.JsonToSensorData(message);
			logger.info("Printing SensorData:\n");
			System.out.println("The sensor Data is :\n" + sensor);
		} catch (Exception ex) {
			ex.printStackTrace();
		} 
		return true;	
	}

}