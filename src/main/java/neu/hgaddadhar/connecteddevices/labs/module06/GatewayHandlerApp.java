package neu.hgaddadhar.connecteddevices.labs.module06;

/*
 * This is he main class which will call the GatewayDataManager
 */
public class GatewayHandlerApp {
	public static void main(String[] args)throws InterruptedException {
		GatewayDataManager Manager = new GatewayDataManager();
		Manager.run();
	}
}