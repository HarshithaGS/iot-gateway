package neu.hgaddadhar.connecteddevices.labs.module06;


import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import neu.hgaddadhar.connecteddevices.common.ConfigConst;
import neu.hgaddadhar.connecteddevices.common.SensorData;

//This class contain the reference to the MQTT client library (Eclipse Paho)
public class MqttClientConnector implements MqttCallback {

	private static final Logger logger = Logger.getLogger(MqttClientConnector.class.getName());
	private String protocol = ConfigConst.DEFAULT_MQTT_PROTOCOL;
	private String host = ConfigConst.DEFAULT_MQTT_SERVER;
	private int port = ConfigConst.DEFAULT_MQTT_PORT;

	private String clientID;
	private String brokerAddr;
	private MqttClient mqttClient;
	 SensorData sensorData;
	private static String messag;

	/*
	 * Constructor to create an object
	 */
	public MqttClientConnector() {
		if (host != null && host.trim().length() > 0) {
			this.sensorData = new SensorData();
			this.host = host;
			this.clientID = MqttClient.generateClientId();
			logger.info("Using client id for broker connection: " + clientID);
			this.brokerAddr = protocol + "://" + host + ":" + port;
			logger.info("URL used for brokr connection: " + brokerAddr);
		}
	}

	//This method references to the initiating of the MQTT connection
	public void connect() {
		if (mqttClient == null) {
			MemoryPersistence persistence = new MemoryPersistence();
			try {
				mqttClient = new MqttClient(brokerAddr, clientID, persistence);
				MqttConnectOptions connOpts = new MqttConnectOptions();//Holds the set of options that control how the client connects to a server.
				connOpts.setCleanSession(true);
				logger.info("Connecting to broker: " + brokerAddr);
				mqttClient.setCallback(this);
				mqttClient.connect(connOpts);
				logger.info("connected to broker: " + brokerAddr);
			} catch (MqttException ex) {
				logger.log(Level.SEVERE, "Failed to connect to broker" + brokerAddr, ex);
			}

		}
	}
	//This method disconnects or closes the connection
	public void disconnect() {
		try {
			mqttClient.disconnect();
			logger.info("Disconnecting  from the  broker: " + brokerAddr);
		} catch (Exception ex) {
			logger.log(Level.SEVERE, "Failed to disconnect from broker: " + brokerAddr, ex);
		}
	}

	//This is the method taht will help to subscribe to the topic asigned
	public boolean subscribeToTopic(String topic, int qos) {
		boolean success = false;
		try {
			mqttClient.subscribe(topic, qos);
			success = true;
			//mqttClient.unsubscribe(topic);
		} catch (MqttException e) {
			e.printStackTrace();
		}

		return success;
	}
	//This method is called if the connection is not possibel to set up
	public void connectionLost(Throwable cause) {

		logger.log(Level.WARNING, "Connection to broker lost. Will retry soon.", cause);
	}
	
	//This method is to be called when the mesage is arrived at the subscriber
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		  MqttClientConnector.setMessag(message); 
		  logger.info("Message arrived: " +topic + "," + message.getId() + "\n"); 
		  System.out.print("Message arrived: " +topic + "," + message.getId() + message + "\n");
	}

	//This method is called when data is delivered completely
	public void deliveryComplete(IMqttDeliveryToken token) {
		logger.info("Delevery Complete: " + token.getMessageId() + "-" + token.getResponse());
	}

	//This method returns the message 
	public static String getMessag() {
		return messag;
	}
	//sets the message in String format
	public static void setMessag(MqttMessage message) {
		MqttClientConnector.messag = message.toString();
	}
	//Default implemented callback method
	public void setCallback(GatewayDataManager GatewayDataManager) {
		// TODO Auto-generated method stub

	}

}