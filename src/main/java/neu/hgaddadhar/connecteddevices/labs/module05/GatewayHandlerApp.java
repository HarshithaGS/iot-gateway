package neu.hgaddadhar.connecteddevices.labs.module05;

import neu.hgaddadhar.connecteddevices.labs.module05.GatewayDataManager;

/*
 * GateWayHandlerApp is handling all the connections and instantiating GatewayDataManager class
 */
public class GatewayHandlerApp {

	public static void main(String args[]) throws InterruptedException {
		GatewayDataManager manage=new GatewayDataManager();
		manage.Manager();
	}
}
