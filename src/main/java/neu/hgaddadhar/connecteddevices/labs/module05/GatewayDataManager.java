package neu.hgaddadhar.connecteddevices.labs.module05;
 
//imports
import neu.hgaddadhar.connecteddevices.common.ActuatorData;
import neu.hgaddadhar.connecteddevices.common.SensorData;
import neu.hgaddadhar.connecteddevices.common.PersistenceUtil;
import neu.hgaddadhar.connecteddevices.common.SensorDataListener;
import neu.hgaddadhar.connecteddevices.common.ActuatorDataListener;
import neu.hgaddadhar.connecteddevices.common.DataUtil;
import neu.hgaddadhar.connecteddevices.common.ConfigConst;
import neu.hgaddadhar.connecteddevices.common.ConfigUtil;


/*
 * This class handles the ACtuation of Sensor Data and restoring of data into the Redis * 
 */
public class GatewayDataManager {

	
	//Instance of Classes
	PersistenceUtil putil=new PersistenceUtil();
	DataUtil dutil = new DataUtil();
	SensorData s=new SensorData();
	ActuatorData a = new ActuatorData();
	SensorDataListener sdl = new SensorDataListener();
	ActuatorDataListener adl = new ActuatorDataListener();


	boolean test = ConfigUtil.getInstance().loadConfig(ConfigConst.DEFAULT_CONFIG_FILE_NAME);

	Float nominalval= Float.valueOf((ConfigUtil.getInstance().getProperty(ConfigConst.CONSTRAINED_DEVICE, ConfigConst.NOMINAL_VAL)));

	//Reads the Sensor Data and compares with the Nominal Value and based on condition generates the actuated data 
	public boolean Manager()
	{
		for (int i=1;i<=12;i++) {
			s=putil.ReadFromRedis();
			if (s.getName() .equals( "Temperature Reading is") ){
				a.setName("Temperature Reading is");
			}
			else if (s.getName() .equals( "Humidity Reading is"))
			{
				a.setName("Humidity Reading is");
			}
			else
			{
				a.setName("I2c Reading is");
			}
			sdl.onMessage();

			float new_val=s.getcurValue();
			System.out.println("new value"+new_val);
			if (new_val > nominalval) {
				a.setCommand(a.COMMAND_OFF);
				a.setStatuscode(a.STATUS_ACTIVE);
				a.setErrcode(a.ERROR_OK);
				a.setStateData("Decrease");
				a.setVal(Math.abs(new_val - nominalval));
				putil.WriteToRedis((a));
			} else {
				a.setCommand(a.COMMAND_ON);
				a.setStatuscode(a.STATUS_ACTIVE);
				a.setErrcode(a.ERROR_OK);
				a.setStateData("Increase");
				a.setVal(Math.abs(new_val - nominalval));
				putil.WriteToRedis(a);
			}
		}
		//listener to hold a flag to ping python device multiactuatoradaptor
		adl.onMessage();
		putil.WriteToRedisFlag("done");
		return true;
	}
}
