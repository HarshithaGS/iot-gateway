package neu.hgaddadhar.connecteddevices.labs.module09;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import neu.hgaddadhar.connecteddevices.common.SensorData;



import neu.hgaddadhar.connecteddevices.common.ConfigConst;
import neu.hgaddadhar.connecteddevices.common.DataUtil;


/**
 * @author HarshithaGS
 *
 */
public class MqttClientConnector implements MqttCallback {
	/**
	 * implements methods to connect, publish message , subscribe to topic
	 * unsubscribe and disconnect the mqtt client
	 * 
	 * @param: logger --> static Logger
	 * @param: protocol --> protocol used in connection
	 * @param: host --> domain address of mqtt broker
	 * @param: port --> port number
	 * @param: brokerAddr --> the complete address of mqtt broker
	 * @param: clientID --> represent the the client id
	 * @param: sensorData --> instance variable to SensorData object
	 * @param: dataUtil --> instance variable to DataUtil object
	 */
	private static final Logger logger = Logger.getLogger(MqttClientConnector.class.getName());
	private String protocol = ConfigConst.DEFAULT_MQTT_PROTOCOL;
	private String host = ConfigConst.DEFAULT_MQTT_SERVER;
	private int port = ConfigConst.DEFAULT_MQTT_PORT;
	private String authToken;
	private String pemFileName;
	private String clientID;
	private boolean isSecureConn = false;
	private String brokerAddr;
	private MqttClient mqttClient;
	private SensorData sensorData;
	private DataUtil dataUtil;

	/**
	 * MqttClientConnector constructor
	 * 
	 * @param host: Name of the broker to connect.
	 * @param authToken: Authorizing access to the broker.
	 * @param pemFileName: The name of the certificate file to use. If null /
	 *        invalid, ignored.
	 */
	public MqttClientConnector(String host, String authToken, String pemFileName) {

		super();
		if (host != null && host.trim().length() > 0) {
			this.host = host;
		}

		if (authToken != null && authToken.trim().length() > 0) {
			this.authToken = authToken;
		}
		if (pemFileName != null) {
			File file = new File(pemFileName);
			if (file.exists()) {
				this.protocol = ConfigConst.SECURE_MQTT_PROTOCOL;
				this.port = ConfigConst.SECURE_MQTT_PORT;
				this.pemFileName = pemFileName;
				this.isSecureConn = true;
				logger.info("PEM file valid. Using secure connection: " + pemFileName);
			} else {
				logger.warning("PEM file invalid. Using insecure connection: " + pemFileName);
			}
		}
		this.clientID = MqttClient.generateClientId();
		this.brokerAddr = this.protocol + "://" + this.host + ":" + this.port;
		logger.info("Using URL for broker conn: " + brokerAddr);
	}

	/**
	 * Constructor
	 */
	public MqttClientConnector() {
		if (host != null && host.trim().length() > 0) {
			this.sensorData = new SensorData();
			this.dataUtil = new DataUtil();
			this.clientID = mqttClient.generateClientId();
			logger.info("Using client id for broker connection: " + clientID);
			this.brokerAddr = protocol + "://" + host + ":" + port;
			logger.info("Using URL for broker connection: " + brokerAddr);
		}
	}

	/**
	 * connect to mqqt broker
	 */
	public void connect() {
		if (mqttClient == null) {
			MemoryPersistence persistence = new MemoryPersistence();
			try {
				// MqqtClient object
				mqttClient = new MqttClient(brokerAddr, clientID, persistence);
				// MqttConnectOptions object holds set of options that control how client
				// connects
				MqttConnectOptions connOpts = new MqttConnectOptions();
				connOpts.setCleanSession(true);
				if (authToken != null)
					connOpts.setUserName(authToken);
				if (isSecureConn)
					initSecureConnection(connOpts);
					logger.info("Connecting to broker: " + brokerAddr);
				// setting the callback
				mqttClient.setCallback(this);
				// connecting to broker
				mqttClient.connect(connOpts);
				logger.info("connected to broker: " + brokerAddr);
			} catch (MqttException ex) {
				logger.log(Level.SEVERE, "Failed to connect to broker" + brokerAddr, ex);
			}
		}
	}

	/**
	 * Disconnect from the broker
	 */
	public void disconnect() {
		try {
			mqttClient.disconnect();
			logger.info("Disconnect from broker: " + brokerAddr);
		} catch (Exception ex) {
			logger.log(Level.SEVERE, "Failed to disconnect from broker: " + brokerAddr, ex);
		}
	}

	/**
	 * publishing message
	 * 
	 * @param: topic --> topic to publish message to
	 * @param: qosLevel --> setting the qos level
	 * @param: payload --. the message to be sent
	 */
	public boolean publishMessage(String topic, int qosLevel, byte[] payload) {
		boolean msgSent = false;
		try {
			logger.info("Publishing message to topic: " + topic);
			// MqttMessage object
			MqttMessage msg = new MqttMessage(payload);
			msg.setQos(qosLevel);
			// publish the message to topic
			mqttClient.publish(topic, msg);
			logger.info("Message Published " + msg.getId());
			msgSent = true;
		} catch (Exception ex) {
			logger.log(Level.SEVERE, "Failed to publish Mqtt message " + ex.getMessage());
		}
		return msgSent;
	}

	/**
	 * Subscribe to a topic
	 * 
	 * @param: topic --> topic name to subscribe to
	 */
	public boolean subscribeToTopic(String topic,int qos) {
		boolean success = false;
		try {
			mqttClient.subscribe(topic,qos);
			success = true;
		} catch (MqttException e) {
			e.printStackTrace();
		}
		return success;
	}

	/**
	 * Unsubscribing from a topic
	 */
	public boolean unSubscibe(String topic) {
		boolean success = false;
		try {
			// unsubscribe call
			mqttClient.unsubscribe(topic);
			success = true;
		} catch (MqttException e) {
			e.printStackTrace();
		}
		return success;
	}

	/**
	 * message to be sent in json format
	 */
	public byte[] msg() {
		sensorData.setCurValue(20);
		sensorData.setAvgValue(15);
		sensorData.setMaxValue(45);
		sensorData.setMinValue(2);
		sensorData.setSamples(7);
		sensorData.setTimeStamp("time");
		sensorData.setTotValue(277);
		// convert sensor data to json
		String jsonMsg = dataUtil.SensorDataToJson(sensorData);
		return jsonMsg.getBytes();
	}

	/**
	 * Callbacks
	 */
	@Override
	public void connectionLost(Throwable cause) {
		logger.log(Level.WARNING, "Connection to broker lost. Will retry soon.", cause);
	}

	/**
	 * called when message arrives from publisher
	 */
	@Override
	public void messageArrived(String topic, MqttMessage message) {
		System.out.println("hello");
		logger.info("Message arrived: " + topic + message.getId() + "\nmessage: " + message);
		MqttClientConnector newmqtt=new MqttClientConnector();
		newmqtt.connect();
		newmqtt.publish("python", message);
	}

	private void publish(String topic, MqttMessage message) {
		// TODO Auto-generated method stub
		try {
			logger.info("Publishing message to topic: " + topic);
			// publish the message to topic
			mqttClient.publish(topic, message);
			logger.info("Message Published " + message.getId());
			
		} catch (Exception ex) {
			logger.log(Level.SEVERE, "Failed to publish Mqtt message " + ex.getMessage());
		}
		
	}

	/**
	 * called when there is successfull messgage published
	 */
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		logger.info("Delivery is  Complete: " + token.getMessageId() + "-" + token.getResponse());
	}

	private void initSecureConnection(MqttConnectOptions connOpts) {
		try {
			logger.info("Configuring TLS...");
			SSLContext sslContext = SSLContext.getInstance("SSL");
			KeyStore keyStore = readCertificate();
			TrustManagerFactory trustManagerFactory = TrustManagerFactory
					.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			trustManagerFactory.init(keyStore);
			sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
			connOpts.setSocketFactory(sslContext.getSocketFactory());
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to initialize secure MQTT connection.", e);
		}
	}

	/**
	 * read the certificate from ubidots pem file and 
	 * return keystore
	 * @return ks: KeyStore --> storage for cryptographic keys and certificates
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 */
	private KeyStore readCertificate()
			throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		FileInputStream fis = new FileInputStream(pemFileName);
		BufferedInputStream bis = new BufferedInputStream(fis);
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		ks.load(null);
		while (bis.available() > 0) {
			Certificate cert = cf.generateCertificate(bis);
			ks.setCertificateEntry("adk_store" + bis.available(), cert);
		}
		return ks;
	}
	public void setCallback(MqttClientConnector MqttClientConnector) {
		// TODO Auto-generated method stub

	}
}
