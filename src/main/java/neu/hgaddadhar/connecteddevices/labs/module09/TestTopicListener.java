package neu.hgaddadhar.connecteddevices.labs.module09;

import com.amazonaws.services.iot.client.AWSIotMessage;
import com.amazonaws.services.iot.client.AWSIotQos;
import com.amazonaws.services.iot.client.AWSIotTopic;
import com.google.gson.Gson;
import neu.hgaddadhar.connecteddevices.labs.module09.MqttClientConnector;

/**
 * This class extends {@link AWSIotTopic} to receive messages from a subscribed
 * topic.
 */

public class TestTopicListener extends AWSIotTopic {
	MqttClientConnector mqttClient=new MqttClientConnector();
	public byte[] messagenew;




	public void start(String topicName) {

		mqttClient.connect();
		mqttClient.publishMessage("aws", 1,messagenew);
		System.out.println(2);
		try {
			Thread.sleep(6);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		;
	}
	public String msgToJson(String string)
	{
		/**
		 * @param: jsonSd --> String to store the json data
		 * @param: gson --> Gson object
		 * @param: sensordata: Sensor data passed as method argument
		 * @Return: string --> json having sensor data
		 */
		String jsonSd;
		Gson gson = new Gson();
		jsonSd = gson.toJson(string);
		return jsonSd;
	}

	public TestTopicListener(String topic, AWSIotQos qos) {
		super(topic, qos);
	}

	@Override
	public void onMessage(AWSIotMessage message) {
		messagenew=msgToJson(message.getStringPayload()).getBytes();
		start("aws");
		System.out.println("<< " + message.getStringPayload());

	}

}