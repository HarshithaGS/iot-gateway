package neu.hgaddadhar.connecteddevices.labs.module09;

import com.amazonaws.services.iot.client.AWSIotMqttClient;
import com.amazonaws.services.iot.client.AWSIotQos;
import com.amazonaws.services.iot.client.AWSIotTopic;
import neu.hgaddadhar.connecteddevices.labs.module09.AwsSubscriber;
import neu.hgaddadhar.connecteddevices.labs.module09.TestTopicListener;

/*
 *  Main application that runs the AwsSubscriber instance
 *  AWSIot topic is set here and subscriber server is started to listen to that topic
 *  @var TestTopicQos: define qos for subscription
 *  @var TestTopic: Topic name to subscribe
 * 
 * @author HarshithaGS 
 *
 */
public class GatewayHandlerApp {
	private static final String TestTopic = "TempData";
	private static final AWSIotQos TestTopicQos = AWSIotQos.QOS0;
	private static AWSIotMqttClient awsIotClient;

	public static void main(String[] args)
	{
		System.out.println("Server Started Running...");

		try {
			AwsSubscriber sub = new AwsSubscriber();
			awsIotClient=sub.initClient();
			AWSIotTopic topic = new TestTopicListener(TestTopic, TestTopicQos);
			TestTopicListener test = new TestTopicListener(TestTopic, TestTopicQos);

			awsIotClient.subscribe(topic, true);
			test.start("aws");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}