package neu.hgaddadhar.connecteddevices.labs.module09;


import com.amazonaws.services.iot.client.AWSIotMqttClient;
import com.amazonaws.services.iot.client.AWSIotException;
import com.amazonaws.services.iot.client.sample.sampleUtil.SampleUtil;
import com.amazonaws.services.iot.client.sample.sampleUtil.SampleUtil.KeyStorePasswordPair;


public class AwsSubscriber {

	//This is an example that uses AWSIotMqttClient to connect to AWS IOT client and subscribe to a topic


	private static AWSIotMqttClient awsIotClient;

	public static void setClient(AWSIotMqttClient client) {
		awsIotClient = client;
	}

	public static AWSIotMqttClient initClient() throws AWSIotException {
		String clientEndpoint = "aqg86h4a4i5jr-ats.iot.us-east-2.amazonaws.com";   // host Connection Endpoint:iot of your own
		String clientId = "ExtraHW";
		// Thing name that matches with your own client ID.
		String certificateFile ="C:\\Users\\HarshithaGS\\Desktop\\AWSExtraHW\\ee30e7c98b-certificate.pem.crt";     //certificate file
		String privateKeyFile = "C:\\Users\\HarshithaGS\\Desktop\\AWSExtraHW\\ee30e7c98b-private.pem.key";         // PEM encoded private key file

		//KeyStore pair for password authentication is activated
		KeyStorePasswordPair pair = SampleUtil.getKeyStorePasswordPair(certificateFile, privateKeyFile);
		//Client connection
		AWSIotMqttClient client = new AWSIotMqttClient(clientEndpoint, clientId, pair.keyStore, pair.keyPassword);

		// parameters can be set before connect()
		client.connect();
		setClient(client);
		return awsIotClient;
	}
}
