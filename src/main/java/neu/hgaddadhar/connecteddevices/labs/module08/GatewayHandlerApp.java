package neu.hgaddadhar.connecteddevices.labs.module08;


/**
 * @author HarshithaGS
 */

import neu.hgaddadhar.connecteddevices.common.ConfigUtil;



//This is the main class
public class GatewayHandlerApp {

	/*
	 * @var  UBIDOTS_VARIABLE_LABEL = actuator data label name on Ubidots
	 * @var  UBIDOTS_DEVICE_LABEL = Device label name
	 */
	public static final String UBIDOTS_VARIABLE_LABEL = "/tempactuator";
	public static final String UBIDOTS_DEVICE_LABEL = "/deviceapi";
	public static final String UBIDOTS_TOPIC_DEFAULT = "/v1.6/devices" + UBIDOTS_DEVICE_LABEL + UBIDOTS_VARIABLE_LABEL+"/lv";


	public static void main(String[] args)
	{
		System.out.println("Server Started Running...");

		ConfigUtil config = ConfigUtil.getInstance();
		config.loadConfig();

		TempActuatorSubscriberApp tempactSub = new TempActuatorSubscriberApp();
		tempactSub.start(UBIDOTS_TOPIC_DEFAULT);

	}
}

