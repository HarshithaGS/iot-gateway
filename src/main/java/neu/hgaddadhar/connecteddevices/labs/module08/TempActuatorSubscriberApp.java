package neu.hgaddadhar.connecteddevices.labs.module08;

import neu.hgaddadhar.connecteddevices.common.ConfigConst;



import neu.hgaddadhar.connecteddevices.labs.module08.MqttClientConnector;


// This class is used to subscribe for the actuator data from the Ubidots and publish via the local MQTT
public class TempActuatorSubscriberApp {

	/**
	 * Receive message using MQTT protocol (subscribes to Mqtt Topic) 
	 * 
	 * @var subscriberApp: instance variable for TempActuatorSubscriberApp
	 * @var mqttClient: instance variable for MqqtClientConnector
	 * @var host: Broker address for Mqtt
	 * @var pemfileName: file location of the Ubidots pem file
	 * @var UBIDOTS_VARIABLE_LABEL: string constant for ubidots variable name
	 * @var UBIDOTS_VARIABLE_LABEL: String constant for ubidots Device name
	 * @var UBIDOTS_TOPIC_DEFAULT: Topic name to subscribe/publish
	 */
	private static TempActuatorSubscriberApp subscriberApp;
	private MqttClientConnector mqttClient;
	private String host = ConfigConst.DEFAULT_UBIDOTS_SERVER;
	private String _pemFileName = ConfigConst._pemFileName
			+ ConfigConst.UBIDOTS + ConfigConst.CERT_FILE_EXT;
	private String _authToken = ConfigConst._authToken;

	public static final String UBIDOTS_VARIABLE_LABEL = "/tempactuator";
	public static final String UBIDOTS_DEVICE_LABEL = "/deviceapi";
	public static final String UBIDOTS_TOPIC_DEFAULT = "/v1.6/devices" + UBIDOTS_DEVICE_LABEL + UBIDOTS_VARIABLE_LABEL+"/lv";

	/**
	 * MqttSubClientTestApp constructor
	 */
	public TempActuatorSubscriberApp() {
		super();
	}

	/**
	 * method to connect mqqt client to broker and publish the message
	 * @param topicName: name of the MQTT session topic in string
	 * @return 
	 * 
	 */
	public boolean start(String topicName) {
		try {
			mqttClient = new MqttClientConnector(host, _authToken, _pemFileName);
			mqttClient.setCallback(new MqttClientConnector());
			mqttClient.connect();
			
				mqttClient.subscribeToTopic(topicName,1);
				Thread.sleep(60000);
			}
		 catch (Exception e) {
			e.printStackTrace();
			mqttClient.disconnect();
		}
		return true;
	}
	
}