package neu.hgaddadhar.connecteddevices.labs.module08;

import java.io.BufferedReader;



import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import neu.hgaddadhar.connecteddevices.common.ConfigConst;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;



import java.util.logging.Logger;

//	This  class is to publish message using HTTP protocol to Ubidots cloud

public class TempSensorCloudPublisherApp {
	/**
	 * @var baseUrl: the URL for the Ubidots broker
	 * @var cloudPublisher: instance variable for TempSensorCloudPublisherApp
	 * @var tokenHeader: Map(string:String) for storing HTTP token
	 */

	public static final String DEFAULT_BASE_URL = "https://industrial.api.ubidots.com/api/v1.6/";
	private static final Logger logger = Logger.getLogger(TempSensorPublisherApp.class.getName());
	private String baseUrl;
	private Map<String, String> tokenHeader;
	public static TempSensorCloudPublisherApp cloudPublisher;

	/**
	 * Constructor
	 * 
	 * @param token: Unique string token for accessing Ubidots
	 */
	TempSensorCloudPublisherApp(String token) {
		baseUrl = DEFAULT_BASE_URL;
		tokenHeader = new HashMap<>();
		tokenHeader.put("X-AUTH-TOKEN", token);
	}

	private Map<String, String> getCustomHeaders() {
		Map<String, String> customHeaders = new HashMap<>();
		customHeaders.put("content-type", "application/json");
		return customHeaders;
	}

	/**
	 * Add headres to the MAP
	 * 
	 * @return headers
	 */
	private Map<String, String> prepareHeaders(Map<String, String> arg) {
		Map<String, String> headers = new HashMap<>();
		headers.putAll(getCustomHeaders());
		headers.putAll(arg);
		return headers;
	}

	/**
	 * GET request on the API with a given path
	 * 
	 * @param path: Path to append to the base URL.
	 * @param customParams: Custom parameters added by the user
	 * @return Response from the API. The API sends back data encoded in JSON.
	 */
	String get(String path, Map<String, String> customParams) {
		String response = null; // return variable
		Map<String, String> headers = prepareHeaders(tokenHeader);
		if (customParams != null) {
			List<NameValuePair> params = new LinkedList<>();

			for (Map.Entry<String, String> entry : customParams.entrySet()) {
				params.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}
			path.concat("?");
			path.concat(URLEncodedUtils.format(params, "utf8"));
		}
		try (CloseableHttpClient client = HttpClientBuilder.create().build()) {
			String url = baseUrl + path;
			HttpGet get = new HttpGet(url);
			for (String name : headers.keySet()) {
				get.setHeader(name, headers.get(name));
			}
			HttpResponse resp = client.execute(get);
			BufferedReader rd = new BufferedReader(new InputStreamReader(resp.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line;

			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			response = result.toString();

			// We just need the result field
			JsonParser parser = new JsonParser();
			JsonObject jsonObject = parser.parse(response).getAsJsonObject();
			if (jsonObject.has("results")) {
				response = jsonObject.get("results").toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	/**
	 * POST request on the API with a given path
	 * 
	 * @param      path: Path to append to the base URL.
	 * @param json String of data encoded in JSON
	 * @return Response from the API
	 */
	String post(String path, String json) {
		String response = null; // return variable
		Map<String, String> headers = prepareHeaders(tokenHeader);

		try (CloseableHttpClient client = HttpClientBuilder.create().build()) {
			String url = baseUrl + path;

			HttpPost post = new HttpPost(url);

			for (String name : headers.keySet()) {
				post.setHeader(name, headers.get(name));
			}

			post.setEntity(new StringEntity(json));
			HttpResponse resp = client.execute(post);

			BufferedReader rd = new BufferedReader(new InputStreamReader(resp.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line;

			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			response = result.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	/**
	 * DELETE request on the API with a given path
	 * 
	 * @param path: Path to append to the base URL.
	 * @return Response from the API
	 */
	String delete(String path) {
		String response = null; // return variable
		Map<String, String> headers = prepareHeaders(tokenHeader);

		try (CloseableHttpClient client = HttpClientBuilder.create().build()) {
			String url = baseUrl + path;

			HttpDelete delete = new HttpDelete(url);

			for (String name : headers.keySet()) {
				delete.setHeader(name, headers.get(name));
			}

			HttpResponse resp = client.execute(delete);

			if (resp.getEntity() == null) {
				response = "";
			} else {
				BufferedReader rd = new BufferedReader(new InputStreamReader(resp.getEntity().getContent()));

				StringBuffer result = new StringBuffer();
				String line;

				while ((line = rd.readLine()) != null) {
					result.append(line);
				}

				response = result.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * generates random float number between min and max
	 * 
	 * @param min: minimum value to be generated
	 * @param max: maximum valur to be generated
	 * @return float number converted to string
	 */
	public String generateRandomvalue(float min, float max) {
		Random r = new Random();
		float random = min + r.nextFloat() * (max - min);
		logger.info("Temperature Reading : " + Float.toString(random));
		return Float.toString(random);
	}

	/**
	 * Main method for TempSensorCloudPublishApp class
	 */
	public static void main(String[] args) {
		final String UBIDOTS_PATH_SEPERATOR = "/";
		final String UBIDOTS_VARIABLE_LABEL = "tempsensorHTTP";
		final String UBIDOTS_DEVICE_LABEL = "deviceapi";
		final String UBIDOTS_TOPIC_DEFAULT = "devices" + UBIDOTS_PATH_SEPERATOR + UBIDOTS_DEVICE_LABEL;
		String authToken = ConfigConst._authToken;		
		float min = 0f;
		float max = 45f;
		try {
			cloudPublisher = new TempSensorCloudPublisherApp(authToken);
			JSONObject Jobj = new JSONObject();
			while (true){
				Jobj.put(UBIDOTS_VARIABLE_LABEL, cloudPublisher.generateRandomvalue(min, max));
				String json = Jobj.toString();
				cloudPublisher.post(UBIDOTS_TOPIC_DEFAULT, json);
				Thread.sleep(60000);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
