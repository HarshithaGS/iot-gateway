package neu.hgaddadhar.connecteddevices.labs.module07;

/**
 * @author HarshithaGS
 */

import java.net.SocketException;
import neu.hgaddadhar.connecteddevices.common.ConfigUtil;

//This is the main class
public class GatewayHandlerApp {
	public static void main(String[] args)
	{
		System.out.println("Server Started Running...");

		ConfigUtil config = ConfigUtil.getInstance();
		config.loadConfig();

		try {
			CoapServerManager coapServer = new CoapServerManager(config);
			coapServer.start();
		} catch (SocketException e) {
			e.printStackTrace();
		}

	}
}

