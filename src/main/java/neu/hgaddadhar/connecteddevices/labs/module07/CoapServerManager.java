package neu.hgaddadhar.connecteddevices.labs.module07;

/**
 * @author HARSHITHAGS
 * */

//Imports
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.EndpointManager;
import neu.hgaddadhar.connecteddevices.common.ConfigConst;
import neu.hgaddadhar.connecteddevices.common.ConfigUtil;

// This class helps in insitiating the channels used in the coap connection
public class CoapServerManager extends CoapServer {
	private final int COAP_PORT;

	public CoapServerManager(ConfigUtil config) throws SocketException {
		COAP_PORT = config.getIntegerProperty(ConfigConst.COAP_GATEWAY_SECTION, ConfigConst.PORT_KEY);
		//Adding  the endpoints and the server resources while initializing the server
		addEndpoints();
		add(new TempResourceHandler());
		add(new HumidityResourceHandler());		
	}

	/**
	 * This is the method that is used to bind the CoAP server to the necessary endpoints
	 */
	private void addEndpoints()
	{
		for (InetAddress addr : EndpointManager.getEndpointManager().getNetworkInterfaces()) {
			// only binds to localhost
			if (addr.isLoopbackAddress()) {
				InetSocketAddress bindToAddress = new InetSocketAddress(addr, COAP_PORT);
				addEndpoint(new CoapEndpoint(bindToAddress));
			}
		}
	}
}
