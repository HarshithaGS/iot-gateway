package neu.hgaddadhar.connecteddevices.labs.module02;

/* 
 * This the Main App class which will initiates the SystemPerformanceAdapter Class 
 */
public class GatewayHandlerApp 
{


	// constructors
	public GatewayHandlerApp() { super(); }

	public GatewayHandlerApp(String appName) { super(); }

	//App Start method
	private void startApp() 
	{	//instance of TempEmulatorAdaptor
		TempEmulatorAdaptor spa = new TempEmulatorAdaptor() ;


	}

	//MAIN method
	public static void main(String[] args)
	{
		GatewayHandlerApp app = new GatewayHandlerApp(GatewayHandlerApp.class.getSimpleName());
		app.startApp();
	}	
}