package neu.hgaddadhar.connecteddevices.labs.module02;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import neu.hgaddadhar.connecteddevices.common.ConfigConst;
import neu.hgaddadhar.connecteddevices.common.ConfigUtil;

public class SmtpClientConnector {

	private static final Logger _Logger = Logger.getLogger(SmtpClientConnector.class.getSimpleName());

	boolean _isConnected = false;
	Session session = null;
	
	//this method sets the parameters for the connection to take place
	public void connect() {
		final Properties props = new Properties();
		boolean test = ConfigUtil.getInstance().loadConfig(ConfigConst.DEFAULT_CONFIG_FILE_NAME);

		if (!_isConnected) {
			_Logger.info("Initializing SMTP gateway...");


			final String fromEmail=ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.FROM_ADDRESS_KEY);
			final String password=ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.USER_AUTH_TOKEN_KEY);
			final String hostkey = ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.HOST_KEY);
			/*
			 * final String enkey =
			 * ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION,
			 * ConfigConst.ENABLE_AUTH_KEY); final String smtphost =
			 * ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION,
			 * ConfigConst.SMTP_PROP_HOST_KEY); final String smtpport =
			 * ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION,
			 * ConfigConst.SMTP_PROP_PORT_KEY); final String smtpauth =
			 * ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION,
			 * ConfigConst.SMTP_PROP_AUTH_KEY); final String smtptls =
			 * ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION,
			 * ConfigConst.SMTP_PROP_ENABLE_TLS_KEY);
			 */

			// Setup mail server
			props.put("mail.smtp.host", hostkey);
			props.put("mail.smtp.port", 587);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.user", fromEmail);
			props.put("mail.password", password);
			//props.put(ConfigConst.SMTP_PROP_ENABLE_TLS_KEY, ConfigConst.ENABLE_CRYPT_KEY);
			props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

			_Logger.info(props.toString());

			this.session = Session.getInstance(props,new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(fromEmail,password);
				}
			});
			this._isConnected = true;
		} 
		else 
		{
			_Logger.info("SMTP gateway connection already initialized.");
		}
	}

	//THis method will publish the message via SMTP
	public boolean publishMessage(String subject, String payload)
	{
		System.out.println("start publishing");
		if (! this._isConnected) {
			connect(); 
		}
		boolean success = false;
		String fromAddrStr =ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.FROM_ADDRESS_KEY);
		try {
			Message smtpMsg = new MimeMessage(this.session);
			InternetAddress fromAddr = new InternetAddress(fromAddrStr);
			InternetAddress[] toAddr = InternetAddress.parse(ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.TO_ADDRESS_KEY));

			smtpMsg.setFrom(fromAddr);
			smtpMsg.setRecipients(Message.RecipientType.TO, toAddr);
			smtpMsg.setSubject(subject);
			smtpMsg.setText(new String(payload));

			System.out.println("2");

			Transport.send(smtpMsg);

			System.out.println("3");
			success = true;
		} catch (Exception e) {
			_Logger.log(Level.WARNING, "Failed to send SMTP message from address: " + fromAddrStr, e);
		}
		return success;
	}
}


