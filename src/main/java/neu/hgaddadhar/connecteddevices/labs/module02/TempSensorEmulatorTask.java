package neu.hgaddadhar.connecteddevices.labs.module02;

import java.util.logging.Level;
import java.util.logging.Logger;

import neu.hgaddadhar.connecteddevices.common.SensorData;

public class TempSensorEmulatorTask{
	private static final Logger _Logger = Logger.getLogger(TempEmulatorAdaptor.class.getSimpleName());
	SensorData sensorData = new SensorData();

	double THRESHOLD = 5 ;

	// generates random Temp data
	public void Generatesensordata(){

		float min = 0;
		float max = 30;
		float random_number = (float) (Math.random() * (max - min + 1) + min); 
		sensorData.addValue(random_number);
	}

	//Run method which will generate temp data and if exceeds the threshold will send a mail
	public void run ()
	{
		while(true) {
			Generatesensordata();

			if (sensorData.getCurValue() > sensorData.getAvgValue()){//+ THRESHOLD ){
				sendNotification(); }
		}
	}


	//this method will send the notification using SMTP
	public void sendNotification()
	{
		try {
			String subject = "Temperature above threshold";
			// my publishMessage() API takes byte[] as the payload for flexibility
			SmtpClientConnector _smtpConnector = new SmtpClientConnector();
			_smtpConnector.publishMessage(subject, sensorData.toString());
			System.out.println(sensorData.toString());
		} catch (Exception e) {
			_Logger.log(Level.WARNING, "Failed to send SMTP message.", e);
		}
	}
}

