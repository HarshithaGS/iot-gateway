package neu.hgaddadhar.connecteddevices.labs.module01;

/*
 * This Class helps in scheduling the poll cycle to derive the CPU Usage and Memory Usage and display
 */
public class DevicePollingManager  {
	
	//Constructor
	public DevicePollingManager (int n) {
	}

	public void schedulePollingTask(SystemMemUtilTask systemMemUtilTask, long _pollCycle) {
		systemMemUtilTask.getdataFromSensor();
	}

	public void schedulePollingTask(SystemCpuUtilTask systemMemUtilTask, long _pollCycle) {
		systemMemUtilTask.getdataFromSensor();
	}
	
}

