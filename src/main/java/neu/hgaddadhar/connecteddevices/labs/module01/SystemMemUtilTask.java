package neu.hgaddadhar.connecteddevices.labs.module01;

/*
 * ManagementFactory - Provides the management interfaces for monitoring and management of the Java virtual machine and other components in the Java runtime.
 * MemoryMXBean - management interface for the memory system of the Java virtual machine.
 */
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.util.logging.Logger;
 
//Public class to determine the memory usage of the computer
public class SystemMemUtilTask  {
	
	private static final Logger _Logger = Logger.getLogger(SystemMemUtilTask.class.getSimpleName());

	//Constructor
	public SystemMemUtilTask(String name ,long pollCycle) { super(); }

	public float getdataFromSensor()
	{
		MemoryMXBean memBean = ManagementFactory.getMemoryMXBean();
		MemoryUsage heap	 = memBean.getHeapMemoryUsage();
		MemoryUsage nonheap  = memBean.getNonHeapMemoryUsage();
			
		double heapUtil 	= ((double) heap.getUsed() / heap.getCommitted()) * 100d;
		double nonheapUtil 	= ((double) nonheap.getUsed() /nonheap.getCommitted()) * 100d;
				
		if (heapUtil    < 0.0d)    heapUtil 	=   0.0d;
		if (nonheapUtil < 0.0d) nonheapUtil 	=	0.0d;
		
		//_Logger.info("\tHeap Max: " + heap.getMax() + "\tHeap Used : " + heap.getUsed() + "\tHeap Util " + heapUtil);
		//_Logger.info("\tNon-Heap Max: " + nonheap.getMax() + "\tNon-Heap Used : " + nonheap.getUsed() + "\tNon-Heap Util :" + nonheapUtil);
		
		_Logger.info("\t Memory Usage Percentage  " + heapUtil );
		
		
		return (float) heapUtil;
		
	}

}
