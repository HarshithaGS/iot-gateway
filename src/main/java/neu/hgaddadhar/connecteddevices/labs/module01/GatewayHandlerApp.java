package neu.hgaddadhar.connecteddevices.labs.module01;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.eclipse.paho.client.mqttv3.MqttClient;

import neu.hgaddadhar.connecteddevices.project.MqttClientConnector;
import neu.hgaddadhar.connecteddevices.project.common.ConfigConst;
import neu.hgaddadhar.connecteddevices.project.common.DataUtil;
import neu.hgaddadhar.connecteddevices.project.common.SensorData;

/* 
 * This the Main App class which will initiates the SystemPerformanceAdapter Class 
 */
public class GatewayHandlerApp 
{
	private static final Logger logger = Logger.getLogger(MqttClientConnector.class.getName());


	/**
	 * Logger function to Handle all logs and outputs to file JLogFile
	 */
	public void createLogger() {
		FileHandler fileHandler = null;
		try {
			fileHandler = new FileHandler("C:\\Users\\HarshithaGS\\Desktop\\CDLabModules\\iot-gateway\\src\\main\\java\\neu\\hgaddadhar\\connecteddevices\\project\\java.log", true); 
			logger.addHandler(fileHandler);
			SimpleFormatter formatter = new SimpleFormatter(); 
			fileHandler.setFormatter(formatter);  

		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        
	}

	// constructors
	public GatewayHandlerApp() { super(); }
	 
	public GatewayHandlerApp(String appName) { super(); }
	
	//App Start method
	private void startApp() 
	{
		SystemPerformanceAdapter spa = new SystemPerformanceAdapter(10L) ;
		Thread t1 = new Thread(spa); 
        t1.start(); 
        
	}
	
	//MAIN method
	public static void main(String[] args)
	{
		GatewayHandlerApp app = new GatewayHandlerApp(GatewayHandlerApp.class.getSimpleName());
		app.startApp();
	}	
}