package neu.hgaddadhar.connecteddevices.labs.module01;

import java.lang.management.ManagementFactory;
import com.sun.management.OperatingSystemMXBean;
import java.util.logging.Logger;

/*
 * This Class returns the CPU Usage Percentage  using the  function OperatingSystemMXBean 
 */

public class SystemCpuUtilTask  {

private static final Logger _Logger = Logger.getLogger(SystemMemUtilTask.class.getSimpleName());

	public SystemCpuUtilTask(String name ,long pollCycle) {
		super();
	}

	public float getdataFromSensor()
	{
		OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);
		//  This function will load the overall system is ranged from 0.0-1.0
		_Logger.info("\tCPU Usage: " + osBean.getSystemCpuLoad() * 100);
		
		return (float) (osBean.getSystemCpuLoad() * 100 );		
	}
}