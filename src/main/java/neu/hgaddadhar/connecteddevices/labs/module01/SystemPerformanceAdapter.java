package neu.hgaddadhar.connecteddevices.labs.module01;

/*
 * This class displays the  output for each task class
 */
public class SystemPerformanceAdapter  implements Runnable {

	//  declarations
	DevicePollingManager _pollManager;
	long _pollCycle;

	//Constructor
	public SystemPerformanceAdapter(long pollCycle)
	{
		super();
		
		//The pollcycle is set to 1
		if (pollCycle >= 1L) {
			_pollCycle = pollCycle;
		}
		_pollManager = new DevicePollingManager(2);
	}


	//Polling Method for Start
	public void startPolling() {

		//	_Logger.info("Creating and scheduling CPU Utilization poller...");
		_pollManager.schedulePollingTask(new SystemCpuUtilTask("CPU Utilization",_pollCycle),_pollCycle);

		//_Logger.info("Creating and scheduling Memory Utilization poller...");
		_pollManager.schedulePollingTask(new SystemMemUtilTask("Memory Utilization", _pollCycle), _pollCycle); 
	}


	//The Run Method that controls the times interval of the display of the Usage values
	public void run() {

		for (int i =0;i<this._pollCycle;i++) {

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.startPolling();
		}

	}
}