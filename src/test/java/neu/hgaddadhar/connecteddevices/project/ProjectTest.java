/**
 * 
 */
package neu.hgaddadhar.connecteddevices.project;

import org.junit.After;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import neu.hgaddadhar.connecteddevices.project.ActuatorDataSubscriberApp;
import neu.hgaddadhar.connecteddevices.project.MqttClientConnector;
/**
 * Test class for all requisite Project functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app.
 * 5) Include a screen shot of the report when you submit your assignment.
 * 
 * Please note: While some example test cases may be provided, you must write your own for the class.
 */
public class ProjectTest
{
	// setup methods
	ActuatorDataSubscriberApp act_sub;
	MqttClientConnector mqttc;
	
	public static final String UBIDOTS_VARIABLE_TEMPERATURE_LABEL = "tempactuator";
	public static final String UBIDOTS_VARIABLE_PRESSURE_LABEL = "pressureactuator";
	public static final String UBIDOTS_VARIABLE_HUMIDITY_LABEL = "humidityactuator";
	public static final String[] UBIDOTS_VARIABLES = new String[] {UBIDOTS_VARIABLE_TEMPERATURE_LABEL, UBIDOTS_VARIABLE_PRESSURE_LABEL, UBIDOTS_VARIABLE_HUMIDITY_LABEL};
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		act_sub=new ActuatorDataSubscriberApp();
		mqttc = new MqttClientConnector();
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		act_sub =null;
		mqttc =null;
	}
	
	// test methods
	
	/**
	 * 
	 */
	@Test
	public void ActuatorPub()
	{	// THis method test the  functioning MQTT publish method
		mqttc.connect();
		byte[] step="hello Harshitha".getBytes();	
		mqttc.publishMessage("Test", 1, step);
	}
	
	@Test
	public void ActuatorSub()
	{	//This method test the functioning of the Ubidots Subscription
		Assert.assertTrue(true==(act_sub.start(UBIDOTS_VARIABLES)));
	}
	
}
