/**
 * 
 */
package neu.hgaddadhar.connecteddevices.common;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import neu.hgaddadhar.connecteddevices.common.*;
import neu.hgaddadhar.connecteddevices.labs.module05.*;

/**
 * Test class for DataUtil functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app
 * 5) Include a screen shot of the report when you submit your assignment
 * 
 * Please note: While some example test cases may be provided, you must write your own for the class.
 */
public class DataUtilTest
{
	// setup methods
	SensorData sd;
	ActuatorData ad ;
	Gson gson;
	
	@Before
	public void setUp() throws Exception
	{
		ad =new ActuatorData();
		gson = new Gson();
		sd = new SensorData();
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		ad= null;
		gson=null;
	}
	
	// test methods
	
	/**
	 * Testing if ActuatorData is written to Json
	 */
	@Test
	public void testActuatorDataToJson()
	{
		String adJson = gson.toJson(ad);
		System.out.println(adJson);
	}

	
	/**
	 * Testing if SensorData is written to Json
	 */
	@Test
	public void testSensorDataToJson()
	{
		String sdJson = gson.toJson(sd);
		System.out.println(sdJson);
	}
	
	
	
}
