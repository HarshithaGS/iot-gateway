package neu.hgaddadhar.connecteddevices.common;

import org.junit.Assert; 
import org.junit.Test;
import org.junit.After;
import org.junit.Before;


import neu.hgaddadhar.connecteddevices.common.ConfigUtil;

/**
 * Test class for ConfigUtil functionality.
 * 
 * Instructions: 1) Rename 'testSomething()' method such that 'Something' is
 * specific to your needs; add others as needed, beginning each method with
 * 'test...()'. 2) Add the '@Test' annotation to each new 'test...()' method you
 * add. 3) Import the relevant modules and classes to support your tests. 4) Run
 * this class as unit test app. 5) Include a screen shot of the report when you
 * submit your assignment.
 * 
 * Please note: While some example test cases may be provided, you must write
 * your own for the class.
 */

public class ConfigUtilTest { 

	// static
	public static final String DIR_PREFIX = "C:\\Users\\HarshithaGS\\Desktop\\CDLabModules\\iot-gateway\\config\\";
	public static final String TEST_VALID_CFG_FILE = DIR_PREFIX +"ConnectedDevicesConfig.props";

	//initializations
	ConfigUtil cu ; 

	// setup methods
	public void setUp() throws Exception { 
		cu = new ConfigUtil(); 
	}

	@After
	public void tearDown() throws Exception
	{
		cu =null;
	}

	// test methods


	/**
	 * Test method for
	 * {@link neu.hgaddadhar.connecteddevices.common.ConfigUtil#getIntegerProperty(java.lang.String, java.lang.String)}.
	 */

	@Test public void testGetIntegerProperty()
	{
		cu.getInstance().getProperty("smtp.cloud", "port");
	}

	/**
	 * Test method for
	 * {@link neu.hgaddadhar.connecteddevices.common.ConfigUtil#loadConfig(java.lang.String)}.
	 */

	@Test public void testGetProperty()
	{ 
		cu.getInstance().getProperty("smtp.cloud", "host");
	}

	/**
	 * Test method for
	 * {@link neu.hgaddadhar.connecteddevices.common.ConfigUtil#hasProperty(java.lang.String, java.lang.String)}.
	 */

	@Test public void testHasProperty() 
	{
		String s = cu.getInstance().getProperty("smtp.cloud", "host");
		Assert.assertTrue(s != null);
	}


	/**
	 * Test method for 
	 * {@link neu.hgaddadhar.connecteddevices.common.ConfigUtil#isConfigLoaded()}.
	 */
	@Test public void testLoadConfig() {
		Assert.assertTrue(cu.getInstance().loadConfig(TEST_VALID_CFG_FILE));
	}

	/**
	 * Test method for
	 * {@link neu.hgaddadhar.connecteddevices.common.ConfigUtil#isConfigDataLoaded()}.
	 */

	@Test public void testIsConfigDataLoaded() {
		Assert.assertTrue(cu.getInstance().isConfigDataLoaded());  
	}
}


