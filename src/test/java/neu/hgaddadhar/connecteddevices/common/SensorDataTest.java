package neu.hgaddadhar.connecteddevices.common;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import neu.hgaddadhar.connecteddevices.common.SensorData;
/**
 * Test class for SensorData functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app.
 * 5) Include a screen shot of the report when you submit your assignment.
 * 
 * Please note: While some example test cases may be provided, you must write your own for the class.
 */

public class SensorDataTest
{
	//initializations
	SensorData sendata ;

	// setup methods
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		sendata = new SensorData();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		sendata = null;
	}

	// test methods

	/**
	 * Test method for
	 * {@link neu.hgaddadhar.connecteddevices.common.SensorData#isConfigDataLoaded()}.
	 */

	@Test public void testaddValue() 
	{   
		//this tests if a float value can be added to the function
		sendata.addValue(15);

	}


	/**
	 * Test method for
	 * {@link neu.hgaddadhar.connecteddevices.common.SensorData#getAvergaeValue()}.
	 */
	@Test public void testgetAverageValue()
	{
		Assert.assertTrue(sendata.getAvgValue() == (float)(sendata.getAvgValue()));

	}
	/**
	 * Test method for
	 * {@link neu.hgaddadhar.connecteddevices.common.SensorData#getCount()}.
	 */
	@Test public void testgetCount()
	{
		assertTrue(sendata.getSamples() == (int)(sendata.getSamples()));

	}
	/**
	 * Test method for
	 * {@link neu.hgaddadhar.connecteddevices.common.SensorData#getCurrentValue()}.
	 */
	@Test public void testgetCurrentValue()
	{
		assertTrue(sendata.getCurValue() >=0.0f && sendata.getCurValue() <=30.0f);		
	}
	/**
	 * Test method for
	 * {@link neu.hgaddadhar.connecteddevices.common.SensorData#getMaxValue()()}.
	 */
	@Test public void testgetMaxValue()
	{
		assertTrue(sendata.getMaxValue() <= 30.0f);

	}
	/**
	 * Test method for
	 * {@link neu.hgaddadhar.connecteddevices.common.SensorData#getMinValue()}.
	 */
	@Test public void testgetMinValue()
	{		assertTrue(sendata.getMaxValue() >= 0.0f);

	}
	/**
	 * Test method for
	 * {@link neu.hgaddadhar.connecteddevices.common.SensorData#getName()}.
	 */

	@Test public void testgetName()
	{
		assertTrue(sendata.getName() instanceof String);

	}


}
