package neu.hgaddadhar.connecteddevices.labs;

import static org.junit.Assert.assertTrue;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import neu.hgaddadhar.connecteddevices.common.SensorData;
import neu.hgaddadhar.connecteddevices.labs.module02.SmtpClientConnector;

/**
 * Test class for all requisite Module02 functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app.
 * 5) Include a screen shot of the report when you submit your assignment.
 * 
 * Please note: While some example test cases may be provided, you must write your own for the class.
 */
public class Module02Test
	{	
		SensorData sendata;
		SmtpClientConnector smtp;

	// setup methods
	@Before
	public void setUp() throws Exception
	{
		sendata = new SensorData();
		smtp = new SmtpClientConnector();
	}
	@After
	public void tearDown() throws Exception
	{
		sendata =null;
		smtp =null;
	}

	// test methods

	/*
	 * This is a basic test case to check if the mail is sent when the corrrect address and the network data is provided
	 */
	@Test
	public void testSmtpClientConnectorTest() {

		Assert.assertTrue(true== (smtp.publishMessage("TESTING","testing message send".toString())));	
	}


	/*
	 * This test case gives  a reference to the Sensordata stored
	 */
	@Test public void testTempSensorEmulatorTask()
	{
		assertTrue(sendata.toString() instanceof String);
	}

}
