/**
 * 
 */
package neu.hgaddadhar.connecteddevices.labs;

import static org.junit.Assert.fail;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import neu.hgaddadhar.connecteddevices.labs.module01.SystemCpuUtilTask;

/**
 * Test class for all requisite Module01 functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app.
 * 5) Include a screen shot of the report when you submit your assignment.
 * 
 * Please note: While some example test cases may be provided, you must write your own for the class.
 */
public class Module01Test
{
	// setup methods
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
	}
	
	// test methods
	
	/**
	 * 
	 */
	@Test
	public void testSomething()
	{
//		fail("Not yet implemented");
	}
	
	//Test Case to verify the CPU utilization Percentage  
	public void SystemCpuUtilTaskTest()
	{
		SystemCpuUtilTask cpuUsage = new SystemCpuUtilTask(null , 0);
		
		
		System.out.println("CPU Utilization : " +cpuUsage.getdataFromSensor());
		
		if(!(0.0 <=cpuUsage.getdataFromSensor()) && (cpuUsage.getdataFromSensor()<100)){
			fail("Invalid cpu usage data");
		}
	}
	
	//Test Case to verify the Memory Utilization Percentage  
	public void SystemMemUtilTaskTest()
	{
		SystemCpuUtilTask memUsage = new SystemCpuUtilTask(null , 0);
		
		
		System.out.println("Memory  Utilization : " +memUsage.getdataFromSensor());
		
		if(!(0.0 <=memUsage.getdataFromSensor()) && (memUsage.getdataFromSensor()<100)){
			fail("Invalid memory usage data");
		}
	}
	}

