/**
 * 
 */
package neu.hgaddadhar.connecteddevices.labs;

import org.junit.After;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import neu.hgaddadhar.connecteddevices.labs.module06.*;

/**
 * Test class for all requisite Module05 functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app.
 * 5) Include a screen shot of the report when you submit your assignment.
 * 
 * Please note: While some example test cases may be provided, you must write your own for the class.
 */
public class Module06Test
{
	// setup methods
	
	GatewayDataManager gdm ;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		gdm = new GatewayDataManager();
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		gdm = null;
	}
	
	// test methods
	
	/**
	 * Tests if the Manager function is successfully completed and returns a True at the end
	 */
	@Test
	public void testGatewayamanger()
	{
		Assert.assertTrue(true== (gdm.run()));	

	}
	
}
