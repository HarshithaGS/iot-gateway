/**
 * 
 */
package neu.hgaddadhar.connecteddevices.labs;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import neu.hgaddadhar.connecteddevices.labs.module08.TempActuatorSubscriberApp;
import neu.hgaddadhar.connecteddevices.labs.module08.MqttClientConnector;

/**
 * Test class for all requisite Module08 functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app.
 * 5) Include a screen shot of the report when you submit your assignment.
 * 
 * Please note: While some example test cases may be provided, you must write your own for the class.
 */
public class Module08Test
{
	// setup methods
	TempActuatorSubscriberApp act_sub;
	public static final String UBIDOTS_VARIABLE_LABEL = "/tempactuator";
	public static final String UBIDOTS_DEVICE_LABEL = "/deviceapi";
	public static final String UBIDOTS_TOPIC_DEFAULT = "/v1.6/devices" + UBIDOTS_DEVICE_LABEL + UBIDOTS_VARIABLE_LABEL+"/lv";
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		act_sub=new TempActuatorSubscriberApp();
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		act_sub =null;
	}
	
	// test methods
	
	/**
	 * 
	 */
	@Test
	public void ActuatorPubSub()
	{
		Assert.assertTrue(true==(act_sub.start(UBIDOTS_TOPIC_DEFAULT)));
	}
	
}
